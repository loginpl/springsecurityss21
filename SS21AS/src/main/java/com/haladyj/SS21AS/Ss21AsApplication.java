package com.haladyj.SS21AS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss21AsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss21AsApplication.class, args);
	}

}
