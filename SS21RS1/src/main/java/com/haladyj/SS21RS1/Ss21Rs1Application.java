package com.haladyj.SS21RS1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss21Rs1Application {

	public static void main(String[] args) {
		SpringApplication.run(Ss21Rs1Application.class, args);
	}

}
