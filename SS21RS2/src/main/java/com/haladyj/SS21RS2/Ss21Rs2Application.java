package com.haladyj.SS21RS2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss21Rs2Application {

	public static void main(String[] args) {
		SpringApplication.run(Ss21Rs2Application.class, args);
	}

}
